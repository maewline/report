/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.albumproject.service;

import com.mycompany.albumproject.dao.ArtistDao;
import com.mycompany.albumproject.dao.SaleDao;
import com.mycompany.albumproject.model.ReportArtist;
import com.mycompany.albumproject.model.ReportSale;
import java.util.List;

/**
 *
 * @author Arthi
 */
public class ReportService {

    public List<ReportSale> getReportSaleByDay() {
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
    }

    public List<ReportSale> getReportSaleByMonth(int year) {
        SaleDao dao = new SaleDao();
        return dao.getMonthReport(year);
    }

    public List<ReportArtist> getReportArtistByMonth(int year) {
        ArtistDao dao = new ArtistDao();
        return dao.getReportArtistByMonth(year);
    }
}
