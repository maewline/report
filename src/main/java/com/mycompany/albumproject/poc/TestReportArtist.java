/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.albumproject.poc;

import com.mycompany.albumproject.model.ReportArtist;
import com.mycompany.albumproject.model.ReportSale;
import java.util.List;
import com.mycompany.albumproject.service.ReportService;

/**
 *
 * @author Arthi
 */
public class TestReportArtist {

    public static void main(String[] args) {
        ReportService reportService = new ReportService();
        List<ReportArtist> report = reportService.getReportArtistByMonth(2013);
        for (ReportArtist r : report) {
            System.out.println(r);
        }
    }
}
